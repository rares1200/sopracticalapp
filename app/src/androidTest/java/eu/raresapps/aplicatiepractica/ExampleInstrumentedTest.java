package eu.raresapps.aplicatiepractica;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.mock.MockContext;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import eu.raresapps.aplicatiepractica.home.Question;
import eu.raresapps.aplicatiepractica.utils.DBHandler;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    Context context;
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        context = InstrumentationRegistry.getTargetContext();

        assertEquals("eu.raresapps.aplicatiepractica", context.getPackageName());
    }

}
