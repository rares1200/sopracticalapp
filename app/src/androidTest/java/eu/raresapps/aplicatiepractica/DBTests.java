package eu.raresapps.aplicatiepractica;

import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import eu.raresapps.aplicatiepractica.home.Question;
import eu.raresapps.aplicatiepractica.home.QuestionDetail;
import eu.raresapps.aplicatiepractica.utils.DBHandler;

import static org.junit.Assert.*;
/**
 * Created by rares on 01.04.2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class DBTests {
        private DBHandler db;

        @Before
        public void setUp(){
            db = new DBHandler(InstrumentationRegistry.getTargetContext(),null,null,1);
        }

        @After
        public void finish() {
            db.close();
        }

        @Test
        public void testCheckInCacheDB() throws Exception {
            List<Question> questionList = db.getHomeQuestions();
            String qId = questionList.get(0).questionId;
            assertTrue(db.checkInCache(qId));
        }

        @Test
        public void checkAdd() throws Exception{

            QuestionDetail question = new QuestionDetail();
            question.votes = 5;
            question.tag = "android";
            question.ownerName = "me";
            question.content = "content";
            question.title = "title";
            question.questionId = "5899623";


            db.addQuestion(question);
            QuestionDetail result = db.getQuestionDetail(question.questionId);
            assertEquals("5899623",result.questionId);

        }

        @Test
        public void checkUpdate() throws Exception{

            QuestionDetail question = new QuestionDetail();
            question.votes = 5;
            question.tag = "android";
            question.ownerName = "me";
            question.content = "content";
            question.title = "title";
            question.questionId = "5899623";

            db.addQuestion(question);

            question.ownerName = "owner";
            db.addQuestion(question);
            QuestionDetail newResult = db.getQuestionDetail(question.questionId);
            assertEquals("content",newResult.content);
        }

}
