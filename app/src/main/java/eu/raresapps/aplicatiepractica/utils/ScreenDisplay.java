package eu.raresapps.aplicatiepractica.utils;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by rares on 20.03.2017.
 */

public class ScreenDisplay {

    /**
     * Method that displays the screen width in pixels. Used for scaling
     * @param context
     * @return the width of the screen in pixels
     */
    public static int getScreenWidth(Context context){

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels;
    }

}
