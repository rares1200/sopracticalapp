package eu.raresapps.aplicatiepractica.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import eu.raresapps.aplicatiepractica.home.Question;
import eu.raresapps.aplicatiepractica.home.QuestionDetail;

/**
 * Created by rares on 30.03.2017.
 */

public class DBHandler extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "So.db";
    private static final int DATABSE_VERSION = 1;
    private final String CACHE_TABLE_NAME = "CacheTable";

    private final String CACHE_COLUMN_ID = "_id";
    private final String CACHE_QUESTION_ID = "q_id";
    private final String CACHE_QUESTION_VOTES = "q_votes";
    private final String CACHE_QUESTION_TITLE = "q_title";
    private final String CACHE_QUESTION_CONTENT = "q_content";
    private final String CACHE_QUESTION_ANSWER = "q_answer";
    private final String CACHE_PROFILE_PIC = "q_profile_pic";
    private final String CACHE_OWNER_NAME = "q_name";
    private final String CACHE_SEARCH_TAG = "q_tag";
    public DBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context,DATABASE_NAME, factory, DATABSE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query = "CREATE TABLE IF NOT EXISTS " + CACHE_TABLE_NAME + "(" +
                CACHE_COLUMN_ID + " INTEGER PRIMARY KEY," +
                CACHE_QUESTION_ID + " TEXT," +
                CACHE_QUESTION_VOTES + " INTEGER," +
                CACHE_QUESTION_TITLE + " TEXT," +
                CACHE_QUESTION_CONTENT + " TEXT," +
                CACHE_QUESTION_ANSWER  + " TEXT," +
                CACHE_PROFILE_PIC + " TEXT," +
                CACHE_OWNER_NAME + " TEXT," +
                CACHE_SEARCH_TAG + " TEXT);";
        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public QuestionDetail getQuestionDetail(String qId) {

        QuestionDetail detail = new QuestionDetail();
        String query = "SELECT " + CACHE_QUESTION_VOTES + "," +
                CACHE_QUESTION_TITLE + "," +
                CACHE_QUESTION_CONTENT + "," +
                CACHE_QUESTION_ANSWER +
                " FROM " + CACHE_TABLE_NAME +
                " WHERE " + CACHE_QUESTION_ID +
                " = " + qId;
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c != null) {
            c.moveToFirst();
            detail.questionId = qId;
            detail.votes = c.getInt(c.getColumnIndexOrThrow(CACHE_QUESTION_VOTES));
            detail.title = c.getString(c.getColumnIndexOrThrow(CACHE_QUESTION_TITLE));
            detail.content = c.getString(c.getColumnIndexOrThrow(CACHE_QUESTION_CONTENT));
            detail.answer = c.getString(c.getColumnIndexOrThrow(CACHE_QUESTION_ANSWER));
            c.close();
        }

        db.close();
        return detail;
    }

    public List<Question> getHomeQuestions(){

        List<Question> mList = new ArrayList<>();
        String query = "SELECT " + CACHE_QUESTION_ID + "," +
                CACHE_QUESTION_TITLE + "," +
                CACHE_QUESTION_VOTES + "," +
                CACHE_OWNER_NAME + "," +
                CACHE_PROFILE_PIC +
                " FROM " + CACHE_TABLE_NAME +
                " WHERE " + CACHE_SEARCH_TAG + " LIKE \'android\' ORDER BY " + CACHE_QUESTION_ID + " DESC;";

        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(query,null);
        if(c!=null){
            c.moveToFirst();
            for (int i=0;i<c.getCount();i++){

                Question question = new Question(
                        c.getString(c.getColumnIndexOrThrow(CACHE_QUESTION_ID)),
                        c.getString(c.getColumnIndexOrThrow(CACHE_PROFILE_PIC)),
                        c.getString(c.getColumnIndexOrThrow(CACHE_QUESTION_TITLE)),
                        c.getString(c.getColumnIndexOrThrow(CACHE_OWNER_NAME)),
                        c.getInt(c.getColumnIndexOrThrow(CACHE_QUESTION_VOTES))

                );
                mList.add(question);
                c.moveToNext();

            }
            c.close();
        }
        db.close();
        return mList;
    }

    public boolean checkInCache(String qId){

        String query = "SELECT " + CACHE_COLUMN_ID +
                " FROM " + CACHE_TABLE_NAME +
                " WHERE " + CACHE_QUESTION_ID +
                " = " + qId;

        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(query,null);
        if(c!=null){
            if(c.getCount()==1){
                c.close();
                return true;
            }
            c.close();
        }
        db.close();
        return false;
    }

    public List<Question> getQuestionsByTag(String tag){

        List<Question> mList = new ArrayList<>();
        String query = "SELECT " + CACHE_QUESTION_ID + "," +
                CACHE_QUESTION_TITLE + "," +
                CACHE_QUESTION_VOTES + "," +
                CACHE_OWNER_NAME + "," +
                CACHE_PROFILE_PIC +
                " FROM " + CACHE_TABLE_NAME +
                " WHERE " + CACHE_SEARCH_TAG + " LIKE \'" + tag + "\' ORDER BY " + CACHE_QUESTION_ID + " DESC;";

        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(query,null);
        if(c!=null){
            c.moveToFirst();
            for (int i=0;i<c.getCount();i++){

                Question question = new Question(
                        c.getString(c.getColumnIndexOrThrow(CACHE_QUESTION_ID)),
                        c.getString(c.getColumnIndexOrThrow(CACHE_PROFILE_PIC)),
                        c.getString(c.getColumnIndexOrThrow(CACHE_QUESTION_TITLE)),
                        c.getString(c.getColumnIndexOrThrow(CACHE_OWNER_NAME)),
                        c.getInt(c.getColumnIndexOrThrow(CACHE_QUESTION_VOTES))

                );
                mList.add(question);
                c.moveToNext();

            }
            c.close();
        }
        db.close();
        return mList;
    }


    public void addQuestion(QuestionDetail question){

        ContentValues values = new ContentValues();
        values.put(CACHE_QUESTION_ID,question.questionId);
        values.put(CACHE_QUESTION_TITLE,question.title);
        values.put(CACHE_QUESTION_CONTENT,question.content);
        values.put(CACHE_QUESTION_VOTES,question.votes);
        values.put(CACHE_QUESTION_ANSWER,question.answer);
        values.put(CACHE_OWNER_NAME,question.ownerName);
        values.put(CACHE_PROFILE_PIC,question.profilePicture);
        values.put(CACHE_SEARCH_TAG,question.tag);

        if(checkInCache(question.questionId)) {
            SQLiteDatabase db = getWritableDatabase();
            String where = CACHE_QUESTION_ID + "=?";
            String[] whereArgs = new String[]{String.valueOf(question.questionId)};
        }else{
            SQLiteDatabase db = getWritableDatabase();
            db.insert(CACHE_TABLE_NAME, null, values);
            db.close();
        }

    }


}
