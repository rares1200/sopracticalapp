package eu.raresapps.aplicatiepractica.utils;

/**
 * Created by rares on 29.03.2017.
 * Class that defines constants in the app
 */

public class AppConstants {

    public static String KEY_QID = "qid";
    public static String KEY_TAG = "qtag";
    public static String ANDROID = "android";

    public static String URI_HOME_SEARCH = "https://api.stackexchange.com/2.2/questions?page=%1$s&pagesize=50&order=desc&sort=activity&tagged=android&site=stackoverflow";
    public static String URI_QUESTION_ID = "https://api.stackexchange.com/2.2/questions/%1$s?order=desc&sort=activity&site=stackoverflow&filter=!-*f(6rc.lFba";
    public static String URI_ANSWER_ID = "https://api.stackexchange.com/2.2/answers/%1$s?order=desc&sort=activity&site=stackoverflow&filter=!9YdnSMKKT";
    public static String URI_TAG_SEARCH = "https://api.stackexchange.com/2.2/questions?page=%1$s&pagesize=50&order=desc&sort=activity&tagged=%2$s&site=stackoverflow";
    public static String DEBUG_TAG = "Debug";
}
