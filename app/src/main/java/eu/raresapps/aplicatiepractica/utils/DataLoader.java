package eu.raresapps.aplicatiepractica.utils;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * Created by Eu on 13.09.2016.
 * Class that handles some network methods
 */
public class DataLoader {

    /**
     * Method that checks if there is internet connection
     * @param activity context passed as Activity object
     * @return true if there is a connection, false otherwise
     */
    public static boolean isConnection(Activity activity){

        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo!=null && networkInfo.isConnected()){
            return true;
        }else{
            return false;
        }
    }

}
