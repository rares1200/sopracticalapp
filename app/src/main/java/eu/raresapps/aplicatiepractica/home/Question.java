package eu.raresapps.aplicatiepractica.home;

/**
 * Created by rares on 29.03.2017.
 * Class that holds the data for quesion list item
 */

public class Question {

    public String questionId,picture,title,name;
    public int votes;
    public Question(String questionId, String picture, String title, String name, int votes) {
        this.questionId = questionId;
        this.picture = picture;
        this.title = title;
        this.name = name;
        this.votes = votes;
    }
}
