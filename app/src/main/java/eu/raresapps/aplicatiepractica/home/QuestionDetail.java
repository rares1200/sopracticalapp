package eu.raresapps.aplicatiepractica.home;

/**
 * Created by rares on 30.03.2017.
 * Class that holds data for a complete question with an answer, if any
 */

public class QuestionDetail {

    public String questionId,title,content,answer,profilePicture,ownerName,tag;
    public int votes;
    public QuestionDetail(){}

}
