package eu.raresapps.aplicatiepractica.home;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.raresapps.aplicatiepractica.AppClass;
import eu.raresapps.aplicatiepractica.R;
import eu.raresapps.aplicatiepractica.adapters.QuestionAdapter;
import eu.raresapps.aplicatiepractica.content.QuestionDetailActivity;
import eu.raresapps.aplicatiepractica.tags.TagsActivity;
import eu.raresapps.aplicatiepractica.utils.AppConstants;
import eu.raresapps.aplicatiepractica.utils.DBHandler;
import eu.raresapps.aplicatiepractica.utils.DataLoader;
import eu.raresapps.aplicatiepractica.utils.EndlessRecyclerViewScrollListener;

public class HomeScreenActivity extends AppCompatActivity {

    @BindView(R.id.appBar) Toolbar toolbar;
    @BindView(R.id.toolbar_text) TextView toolbarText;
    @BindView(R.id.tag_btn) ImageView tagButton;
    @BindView(R.id.question_list) RecyclerView questionList;


    private Context context;
    private QuestionAdapter mAdapter;
    private List<Question> dataset = new ArrayList<>();
    private ProgressDialog progressDialog;
    private EndlessRecyclerViewScrollListener endlessListener;
    private DBHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        ButterKnife.bind(this);
        context = this;
        db = new DBHandler(context,null,null,1);

        // sets the toolbar and the progress dialog
        setSupportActionBar(toolbar);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading");

        // sets the adapter for question list
        mAdapter = new QuestionAdapter(context, dataset, new QuestionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                 /*
                    Boolean loadNext checks if the details screen should be opened.
                    If there is no connection the question si checked in cache, if is not in cache it will not be opened
                    and a message will be shown
                 */
                boolean loadNext;
                if(!DataLoader.isConnection((Activity)context)){
                    if(db.checkInCache(dataset.get(position).questionId)){
                        loadNext = true;
                    }else{
                        loadNext = false;
                    }
                }else{
                    loadNext = true;
                }
                if(loadNext) {
                    // starts the intent and sends the id of the question and the tag of the question
                    Intent intent = new Intent(context, QuestionDetailActivity.class);
                    intent.putExtra(AppConstants.KEY_QID,dataset.get(position).questionId);
                    intent.putExtra(AppConstants.KEY_TAG,AppConstants.ANDROID);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
                }else{
                    // there is no connection and the question data is not on local database
                    Toast.makeText(context,"Question not available. Please check your connection!",Toast.LENGTH_SHORT).show();
                }

            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        questionList.setLayoutManager(layoutManager);

        questionList.setAdapter(mAdapter);
        /*
            Fetches more data when the user reaches the last item of the current fetched data.
            This is the pagination that acts like an infinte scroll and fetches 50 questions at a time.
         */
        endlessListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                fetchData(page);
            }
        };
        questionList.addOnScrollListener(endlessListener);

        // opens the activity that searches question by a given tag
        tagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,TagsActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        // if there is no connection question list will be loaded from the local databse
        if(!DataLoader.isConnection((Activity)context)){
            dataset.addAll(db.getHomeQuestions());
            mAdapter.notifyDataSetChanged();
            if(dataset.size()==0) {
                Toast.makeText(context, getResources().getString(R.string.no_results), Toast.LENGTH_SHORT).show();
            }
        }else {
            fetchData(1);
        }

    }

    /**
     * Fetches the data from Web API with the tag:android
     * @param page to be fetched
     */
    private void fetchData(int page){

        progressDialog.show();
        // if first page is fetched the list will be cleared
        if(page == 1){
            dataset.clear();
        }

        // setting the uri with page param for Web API
        String uri = String.format(AppConstants.URI_HOME_SEARCH,
                String.valueOf(page));
        StringRequest request = new StringRequest(
                Request.Method.GET,
                uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response){
                        try {

                            // parsing the response from the Web API
                            JSONObject object = new JSONObject(response);
                            JSONArray JSONArrayResponse = new JSONArray(object.getString("items"));
                            for(int i=0;i<JSONArrayResponse.length();i++){

                                String profilePic;
                                String ownerName;
                                JSONObject jsonObject = JSONArrayResponse.getJSONObject(i);

                                JSONObject ownerObj = jsonObject.getJSONObject("owner");
                                profilePic = ownerObj.getString("profile_image");
                                ownerName = ownerObj.getString("display_name");


                                String qId = jsonObject.getString("question_id");
                                String votes = jsonObject.getString("score");
                                String title = jsonObject.getString("title");

                                // question object is created and added to the list
                                dataset.add(new Question(
                                        qId,
                                        profilePic,
                                        title,
                                        ownerName,
                                        Integer.parseInt(votes)

                                ));
                            }
                        }catch (JSONException e){
                            Log.e("Json Error",e.getMessage());
                        }
                        // if no result was found. Also this will be fired if there is a parsing error
                        if(dataset.size()==0) {
                            Toast.makeText(context, getResources().getString(R.string.no_results), Toast.LENGTH_SHORT).show();
                        }
                        mAdapter.notifyDataSetChanged();
                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(dataset.size()==0) {
                            Toast.makeText(context, "No results found", Toast.LENGTH_SHORT).show();
                        }
                        progressDialog.dismiss();
                    }
                }
        );

        AppClass.getInstance().addToRequestQueue(request);

    }

    @Override
    public void onBackPressed() {
        // app is finished with an animation
        finish();
        overridePendingTransition(R.anim.slide_back_in,R.anim.slide_back_out);

    }
}
