package eu.raresapps.aplicatiepractica;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.raresapps.aplicatiepractica.home.HomeScreenActivity;
import eu.raresapps.aplicatiepractica.utils.ScreenDisplay;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.app_logo) ImageView mAppLogo;
    private Handler handler;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        handler = new Handler();
        context = this;

        /* logo is resized to be shown on 80% of the screen width. height will be automatically resized to keep the ratio
            if possible
        */
        mAppLogo.getLayoutParams().width = (int)(0.8 * ScreenDisplay.getScreenWidth(context));

        // the home screen activity will be opened after 1 seconds( 1000 miliseconds)
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(context, HomeScreenActivity.class);
                startActivity(i);
                finish();
                overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
            }
        },1000);

    }
}
