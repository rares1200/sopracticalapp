package eu.raresapps.aplicatiepractica;

import android.app.Application;
import android.content.res.Resources;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender;

/**
 * Created by rares on 29.03.2017.
 */

@ReportsCrashes(
        mailTo = "rares1200@gmail.com", // my email here
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.error_report
)


public class AppClass extends Application {

    public static Resources resources;
    private RequestQueue queue;
    private static AppClass applicationInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        ACRA.init(this);
         if(applicationInstance == null){
            applicationInstance = this;
        }
        resources = getResources();
    }

    /**
     * Method for using only a request queue across application
     * @return RequestQueue
     */
    public RequestQueue getRequestQueue(){
        if(queue == null){
            queue = Volley.newRequestQueue(getApplicationContext());
        }
        return queue;
    }

    /**
     * Method that adds a request to the request queue
     * @param request
     */
    public void addToRequestQueue(Request request){
        getRequestQueue().add(request);
    }

    /**
     * Method to get Singleton Application instance
     * @return Application instance
     */
    public static synchronized AppClass getInstance(){
        return applicationInstance;
    }
}
