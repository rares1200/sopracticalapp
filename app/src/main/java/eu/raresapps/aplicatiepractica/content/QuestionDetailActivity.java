package eu.raresapps.aplicatiepractica.content;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.raresapps.aplicatiepractica.AppClass;
import eu.raresapps.aplicatiepractica.R;
import eu.raresapps.aplicatiepractica.home.QuestionDetail;
import eu.raresapps.aplicatiepractica.utils.AppConstants;
import eu.raresapps.aplicatiepractica.utils.DBHandler;

public class QuestionDetailActivity extends AppCompatActivity {

    @BindView(R.id.appBar) Toolbar toolbar;
    @BindView(R.id.toolbar_text) TextView toolbarText;
    @BindView(R.id.tag_btn) ImageView tagButton;
    @BindView(R.id.tv_votes) TextView tvVotes;
    @BindView(R.id.tv_title) TextView tvTitle;
    @BindView(R.id.tv_content) TextView tvContent;
    @BindView(R.id.layout_answer) LinearLayout layoutAnswer;
    @BindView(R.id.tv_answer_content) TextView tvAnswerContent;

    private Context context;
    private DBHandler db;
    private ProgressDialog progressDialog;
    private String profilePic,ownerName,qId,votes,title,body;
    private boolean isAnswered = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_detail);

        ButterKnife.bind(this);
        context = QuestionDetailActivity.this;
        db = new DBHandler(context,null,null,1);

        // get intent data from list activity
        String questionId = getIntent().getStringExtra(AppConstants.KEY_QID);
        String tag = getIntent().getStringExtra(AppConstants.KEY_TAG);

        // hides the tag button from the toolbar. The same toolbar is used across app except searching activity
        tagButton.setVisibility(View.GONE);

        // creates the dialog for loading data
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(getResources().getString(R.string.loading));

        // sets the toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*
        First the question is searched in cache database and if not found is fetched from Web API
         */

        if(db.checkInCache(questionId)){
            QuestionDetail questionDetail = db.getQuestionDetail(questionId);
            setViews(String.valueOf(questionDetail.votes),questionDetail.title,questionDetail.content,questionDetail.answer);
        }else{
            fetchData(questionId,tag);
        }

    }


    /**
     * Method that fetches data from Web API and caches result in local database
     * @param questionId id of question from list activity
     * @param tag the tag of the question
     */
    private void fetchData(final String questionId, final String tag){

        progressDialog.show();
        // adds param to the API uri
        String uri = String.format(AppConstants.URI_QUESTION_ID,
                questionId);
        StringRequest request = new StringRequest(
                Request.Method.GET,
                uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response){
                        try {
                            //Log.d(AppConstants.DEBUG_TAG,response);

                            // parsing response
                            JSONObject object = new JSONObject(response);
                            JSONArray JSONArrayResponse = new JSONArray(object.getString("items"));

                            for(int i=0;i<JSONArrayResponse.length();i++){
                                profilePic = "";
                                ownerName = "";
                                JSONObject jsonObject = JSONArrayResponse.getJSONObject(i);

                                JSONObject ownerObj = jsonObject.getJSONObject("owner");
                                profilePic = ownerObj.getString("profile_image");
                                ownerName = ownerObj.getString("display_name");


                                qId = jsonObject.getString("question_id");
                                votes = jsonObject.getString("score");
                                title = jsonObject.getString("title");
                                body = jsonObject.getString("body");
                                isAnswered = jsonObject.getBoolean("is_answered");
                                //if the question has an answer it will be fetched from the API
                                if(isAnswered){
                                    String answerId = jsonObject.getString("accepted_answer_id");
                                    fetchAnswer(answerId,questionId,tag);

                                }else{
                                    // question has no answer. The QuestionDetail object is build, the views are set
                                    // and data is added to the local database
                                    QuestionDetail question = new QuestionDetail();
                                    question.questionId = questionId;
                                    question.title = title;
                                    question.answer = "";
                                    question.content = body;
                                    question.ownerName = ownerName;
                                    question.profilePicture = profilePic;
                                    question.tag = tag;
                                    question.votes = Integer.parseInt(votes);
                                    db.addQuestion(question);
                                    setViews(votes,title,body,"");

                                }
                            }
                        }catch (JSONException e){
                            // there was an error on parsing data
                            Log.e("Json Error",e.getMessage());
                            Toast.makeText(context,"Could not get question content",Toast.LENGTH_SHORT).show();
                            finish();
                            overridePendingTransition(R.anim.slide_back_in,R.anim.slide_back_out);
                        }
                        progressDialog.dismiss();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*
                         This should be added if you want to fetch data everytime from Web API and only load data
                         from cache database if there is no internet connection

                        if(!DataLoader.isConnection((Activity)context)) {

                            QuestionDetail questionDetail = db.getQuestionDetail(questionId);
                            setViews(String.valueOf(questionDetail.votes),questionDetail.title,questionDetail.content,questionDetail.answer);
                        }else {
                            Toast.makeText(context, "No results found", Toast.LENGTH_SHORT).show();
                        }*/
                        Toast.makeText(context, "No results found", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }
        );

        AppClass.getInstance().addToRequestQueue(request);

    }

    /**
     * Method that fetches the answer for a questiong given answer id
     * @param answerId id of the accepted answer
     * @param questionId id of the question
     * @param tag tag of the question
     */
    private void fetchAnswer(final String answerId,final String questionId,final String tag){

        // adds param to the API uri
        String uri = String.format(AppConstants.URI_ANSWER_ID,
                answerId);
        StringRequest request = new StringRequest(
                Request.Method.GET,
                uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response){
                        try {
                            // API response is parsed
                            JSONObject object = new JSONObject(response);
                            JSONArray JSONArrayResponse = new JSONArray(object.getString("items"));
                            for(int i=0;i<JSONArrayResponse.length();i++){
                                JSONObject jsonObject = JSONArrayResponse.getJSONObject(i);
                                String answerBody = jsonObject.getString("body");
                                QuestionDetail question = new QuestionDetail();
                                question.questionId = questionId;
                                question.title = title;
                                question.answer = "";
                                question.content = body;
                                question.ownerName = ownerName;
                                question.profilePicture = profilePic;
                                question.tag = tag;
                                question.votes = Integer.parseInt(votes);
                                question.answer = answerBody;
                                // the complete data of the question and answer is added to local database
                                db.addQuestion(question);
                                // views are set with fetched data
                                setViews(votes,title,body,answerBody);

                            }
                        }catch (JSONException e){
                            // error on parsing the data
                            Log.e("Json Error",e.getMessage());
                            Toast.makeText(context,"Could not get question content",Toast.LENGTH_LONG).show();
                            finish();
                            overridePendingTransition(R.anim.slide_back_in,R.anim.slide_back_out);
                        }
                        progressDialog.dismiss();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                }
        );

        AppClass.getInstance().addToRequestQueue(request);

    }

    /**
     * Method that fills the view with data about question
     * @param votes votes of the question
     * @param title title of the question
     * @param content content of the question
     * @param answer answer of the question, if any
     */
    public void setViews(String votes,String title,String content,String answer){

        tvVotes.setText(votes);
        tvTitle.setText(Html.fromHtml(title));
        tvContent.setText(Html.fromHtml(content));
        if(answer!=null && !answer.isEmpty()) {
            tvAnswerContent.setText(Html.fromHtml(answer));
        }else{
            layoutAnswer.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // if back button from toolbar is pressed
        if(item.getItemId() == android.R.id.home){
            finish();
            overridePendingTransition(R.anim.slide_back_in,R.anim.slide_back_out);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_back_in,R.anim.slide_back_out);
    }
}
