package eu.raresapps.aplicatiepractica.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import eu.raresapps.aplicatiepractica.R;
import eu.raresapps.aplicatiepractica.home.Question;

/**
 * Created by rares on 29.03.2017.
 */

public class QuestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Question> dataset = new ArrayList<>();
    private OnItemClickListener mListener;
    private Context context;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.profile_picture) CircleImageView mProfilePicture;
        @BindView(R.id.question_title) TextView mTitle;
        @BindView(R.id.owner) TextView mOwner;
        @BindView(R.id.votes) TextView mVotes;
        @BindView(R.id.layout) RelativeLayout mLayout;

        public ItemViewHolder(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    public QuestionAdapter(Context context, List<Question> dataset, OnItemClickListener mListener) {
        this.dataset = dataset;
        this.mListener = mListener;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_question,null,false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        ItemViewHolder itemHolder = (ItemViewHolder) holder;
        itemHolder.mOwner.setText(dataset.get(position).name);
        itemHolder.mTitle.setText(Html.fromHtml(dataset.get(position).title));
        itemHolder.mVotes.setText(String.valueOf(dataset.get(position).votes));
        Picasso.with(context)
                .load(dataset.get(position).picture)
                .placeholder(context.getResources().getDrawable(R.drawable.ic_person_black_24dp))
                .resize(100,100)
                .into(itemHolder.mProfilePicture);
        itemHolder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}

