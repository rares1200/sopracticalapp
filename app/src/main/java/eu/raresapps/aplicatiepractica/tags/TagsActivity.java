package eu.raresapps.aplicatiepractica.tags;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.raresapps.aplicatiepractica.AppClass;
import eu.raresapps.aplicatiepractica.R;
import eu.raresapps.aplicatiepractica.adapters.QuestionAdapter;
import eu.raresapps.aplicatiepractica.content.QuestionDetailActivity;
import eu.raresapps.aplicatiepractica.home.Question;
import eu.raresapps.aplicatiepractica.utils.AppConstants;
import eu.raresapps.aplicatiepractica.utils.DBHandler;
import eu.raresapps.aplicatiepractica.utils.DataLoader;
import eu.raresapps.aplicatiepractica.utils.EndlessRecyclerViewScrollListener;

public class TagsActivity extends AppCompatActivity {

    @BindView(R.id.appBar) Toolbar toolbar;
    @BindView(R.id.question_list) RecyclerView questionList;


    private Context context;
    private QuestionAdapter mAdapter;
    private List<Question> dataset = new ArrayList<>();
    private ProgressDialog progressDialog;
    private LinearLayoutManager layoutManager;
    private String tag = "";
    private EndlessRecyclerViewScrollListener endlessListener;
    private DBHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tags);

        ButterKnife.bind(this);
        context = this;

        // sets the toolbar and displays the back button
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.entries_search));

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(getResources().getString(R.string.loading));

        db = new DBHandler(context, null, null, 1);

        mAdapter = new QuestionAdapter(context, dataset, new QuestionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                /*
                    Boolean loadNext checks if the details screen should be opened.
                    If there is no connection the question si checked in cache, if is not in cache it will not be opened
                    and a message will be shown
                 */
                boolean loadNext;
                if(!DataLoader.isConnection((Activity)context)){
                    if(db.checkInCache(dataset.get(position).questionId)){
                        loadNext = true;
                    }else{
                        loadNext = false;
                    }
                }else{
                    loadNext = true;
                }
                if(loadNext) {
                    // opens detail activity and send the question id and the tag
                    Intent intent = new Intent(context, QuestionDetailActivity.class);
                    intent.putExtra(AppConstants.KEY_QID, dataset.get(position).questionId);
                    intent.putExtra(AppConstants.KEY_TAG, tag);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                }else{
                    // message shown if there is no connection and data is not in local database
                    Toast.makeText(context,"Question not available. Please check your connection!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        layoutManager = new LinearLayoutManager(context);
        questionList.setLayoutManager(layoutManager);

        questionList.setAdapter(mAdapter);
        // handles the intent for search
        handleIntent(getIntent());
    }

    /**
     * Method that fetches data by page and by tag searched
     * @param page page to be loaded
     * @param tag tag that was searched
     */
    private void fetchData(int page, final String tag){

        progressDialog.show();
        // on the first page the list is cleared
        if(page == 1){
            dataset.clear();
        }
        // uri for API request is set with page and tag params
        String uri = String.format(AppConstants.URI_TAG_SEARCH,
                String.valueOf(page),
                tag);
        StringRequest request = new StringRequest(
                Request.Method.GET,
                uri,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response){
                        try {
                            // parsing the Web API response
                            JSONObject object = new JSONObject(response);
                            JSONArray JSONArrayResponse = new JSONArray(object.getString("items"));

                            for(int i=0;i<JSONArrayResponse.length();i++){
                                String profilePic = "";
                                String ownerName = "";
                                JSONObject jsonObject = JSONArrayResponse.getJSONObject(i);


                                JSONObject ownerObj = jsonObject.getJSONObject("owner");
                                profilePic = ownerObj.getString("profile_image");
                                ownerName = ownerObj.getString("display_name");


                                String qId = jsonObject.getString("question_id");
                                String votes = jsonObject.getString("score");
                                String title = jsonObject.getString("title");
                                dataset.add(new Question(
                                        qId,
                                        profilePic,
                                        title,
                                        ownerName,
                                        Integer.parseInt(votes)
                                ));
                            }
                        }catch (JSONException e){
                            Log.e("Json Error",e.getMessage());
                        }
                        // No results were found
                        if (dataset.size() == 0) {
                            Toast.makeText(context, getResources().getString(R.string.no_results), Toast.LENGTH_SHORT).show();
                        }
                        mAdapter.notifyDataSetChanged();
                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // if there is no connection the data will be fetched from local database
                        if (!DataLoader.isConnection((Activity) context)) {
                            questionList.removeOnScrollListener(endlessListener);
                            dataset.addAll(db.getQuestionsByTag(tag));
                            mAdapter.notifyDataSetChanged();
                        }
                        progressDialog.dismiss();
                        if (dataset.size() == 0) {
                            Toast.makeText(context, getResources().getString(R.string.no_results), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );

        AppClass.getInstance().addToRequestQueue(request);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    /**
     * Method that handles the data that was searched
     * @param intent received when a search is fired
     */
    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            final String query = intent.getStringExtra(SearchManager.QUERY);
            tag = query;
            // fetches data on search action
            fetchData(1,query);
            // infinite scroll is set and acts like pagination loading 50 results at a time
            endlessListener = new EndlessRecyclerViewScrollListener(layoutManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount) {
                    fetchData(page,query);
                }
            };
            questionList.addOnScrollListener(endlessListener);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // adds the search button to toolbar and search functionality
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.options_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // if toolbar back button is pressed the activity will be closed
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slide_back_in, R.anim.slide_back_out);
        }
        return super.onOptionsItemSelected(item);
    }
}
